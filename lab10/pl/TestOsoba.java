import pl.imiajd.oruba.*;
import java.util.*;
import java.time.*;

public class TestOsoba{
	public static void main(String[] arg){
		
		Osoba[] grupa= new Osoba[5];
		
		grupa[0]=new Osoba("ktos",LocalDate.of(2000,10,10));
		grupa[1]=new Osoba("ktos",LocalDate.of(1990,12,12));
		grupa[2]=new Osoba("staszek",LocalDate.of(1881,5,1));
		grupa[3]=new Osoba("zenek",LocalDate.of(1881,5,1));
		grupa[4]=new Osoba("oruba",LocalDate.of(1991,8,3));
	}
}
