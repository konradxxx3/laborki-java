public class PairUtilDemo{
	public static void main(String[] args){
		Pair<Integer> p=new Pair<Integer>(1,2);
		System.out.println(p);
		Pair<Integer> cos=PairUtil.swap(p);
		System.out.println(cos);
		
	}
}

class PairUtil{
	public static <T> Pair<T> swap(Pair<T> para){
		T f=para.getFirst();
		T s=para.getSecond();
		
	return new Pair<T>(s,f);
	}
}
