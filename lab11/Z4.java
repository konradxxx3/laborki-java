import java.time.LocalDate;
import java.util.*;

public class Z4{
    public static void main(String[] args){
        
        Integer[] tab = {1,2,3,4,5,6,7,8,9,10};
        LocalDate[] tab2 = {
            LocalDate.of(2015,7,1),
            LocalDate.of(2015,7,2),
            LocalDate.of(2015,7,3),
            LocalDate.of(2015,7,4),
            LocalDate.of(2015,7,5)
        };
        
        
        
        System.out.println("szukaj 5: " + ArrayUtil.binSearch(tab, 5));
        System.out.println("szukaj 55: " + ArrayUtil.binSearch(tab, 55));
    
        System.out.println("2015,7,1: " + ArrayUtil.binSearch(tab2, LocalDate.of(2015,7,1)));
        System.out.println("2016,7,1: " + ArrayUtil.binSearch(tab2, LocalDate.of(2016,7,1)));   
    }
}    

class ArrayUtil{
    public static <T extends Comparable<T>> boolean binSearch(T[] a, T x){
        
        int i = 0;
        int j = a.length - 1;
        
        while((j - i) > 1){
            int m = (int)((i + j) / 2);
            if(a[m].compareTo(x) <= 0)
                i = m;
            else
                j = m;
        }
        
        if(a[i].compareTo(x) == 0)
            return true;
        else            
            return false;
        
    }
}
