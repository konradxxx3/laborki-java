import java.time.LocalDate;
import java.util.*;

public class Z5{
    public static void main(String[] args){
        
        Integer[] tab = {5,2,7,4,5,6,8,3,11,10};
        LocalDate[] tab2 = {
            LocalDate.of(2015,7,3),
            LocalDate.of(2015,7,2),
            LocalDate.of(2015,8,3),
            LocalDate.of(2015,7,4),
            LocalDate.of(2014,7,5)
        };
        
        
        
        for(int i : tab){
            System.out.print(i + " ");
        }
        System.out.println("");
        
        ArrayUtil.selectionSort(tab);
        
        for(int i : tab){
            System.out.print(i + " ");
        }
        System.out.println("");
        System.out.println("");
        
        for(LocalDate i : tab2){
            System.out.print(i + "    ");
        }
        System.out.println("");
        
        ArrayUtil.selectionSort(tab2);
        
        for(LocalDate i : tab2){
            System.out.print(i + "    ");
        }
        System.out.println("");
    }
}    

class ArrayUtil{
    public static <T extends Comparable<T>> void selectionSort(T[] a){
        for(int i = 0; i < a.length - 1; i++){
            int min = i;
            for(int j = i + 1; j < a.length; j++){
                if(a[j].compareTo(a[min]) < 0){
                    min = j;
                }
            }
            T tmp = a[min];
            a[min] = a[i];
            a[i] = tmp;
        }        
    }
}

