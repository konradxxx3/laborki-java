import java.util.*;

public class Z1{
	public static void main(String[] args){
		
		LinkedList<String> p=new LinkedList<>();
		p.add("ala");
		p.add("lol");
		p.add("mietek");
		p.add("zenek");
		p.add("staszek");
		
		LinkedList<Integer> p1=new LinkedList<>();
		p1.add(1);
		p1.add(2);
		p1.add(3);
		p1.add(4);
		p1.add(5);
		
		System.out.println(p);
		redukuj(p,p.size());
		System.out.println(p);
		System.out.println(p1);
		odwroc(p1);
		System.out.println(p1);

		
	}
	
	public static <T> void redukuj(LinkedList<T> pracownicy, int n){
				
		ListIterator<T> Iter = pracownicy.listIterator();
        while (Iter.hasNext()) {
            Iter.next(); // przesuwa się o jeden element
            if (Iter.hasNext()) {
                Iter.next(); // przesuwa się do następnego elementu
                Iter.remove(); // i usuwa go
            }
        }		
	}
	
	public static <T> void odwroc(LinkedList<T> lista){
		int n=lista.size();
		LinkedList<T> tmp=new LinkedList<>();		
		ListIterator<T> lIter = lista.listIterator();
		ListIterator<T> tIter=tmp.listIterator();
		
		 for(int i=0; i<n; i++)                      
            tmp.add(lista.removeLast());            
        
        lista.clear();
        
        tIter=tmp.listIterator();
        while(tIter.hasNext()){
			lista.add(tIter.next());
		}
     }
}
