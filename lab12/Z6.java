import java.util.*;

public class Z6{
	public static void main(String[] arg){
		Scanner in=new Scanner(System.in);
		Stack<Integer> s=new Stack<>();
		
		int l=in.nextInt();
		
		while (l!=0){
			s.push(l%10);
			l=l/10;			
		}
		
		int n=s.size();
		for(int i=0; i<n; i++)
			System.out.print(s.pop()+", ");
		
		System.out.println();
		
	}
}
