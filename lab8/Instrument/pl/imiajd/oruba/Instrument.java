package pl.imiajd.oruba;
import java.time.LocalDate;

public abstract class Instrument
{
	public Instrument(String producent, LocalDate rokProdukcji){
		this.producent=producent;
		this.rokProdukcji=rokProdukcji;
	}
	
	public String getProducent(){return producent;}	
	public LocalDate getRokProdukcji(){ return rokProdukcji;}
	
	@Override public boolean equals(Object other){
		Instrument inny=(Instrument) other;
		return inny.producent==producent && inny.rokProdukcji==rokProdukcji;
	}
	
	@Override public String toString(){
		return producent+", "+ rokProdukcji;
	}
	
	private String producent;
	private LocalDate rokProdukcji;
}
