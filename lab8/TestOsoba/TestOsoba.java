import pl.imiajd.oruba.*;
import java.util.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] imie = {"Jan", "Andrzej"};

        ludzie[0] = new Pracownik("Kowalski", imie, LocalDate.of(1984,03,06), true, 50000, LocalDate.of(2000,05,12));
        
        String[] imie2 = {"Małgorzata", "Ewa"};
        ludzie[1] = new Student("Nowak", imie2, LocalDate.of(1990,12,24), false, "informatyka", 5.51);

        for (Osoba p : ludzie) {
            System.out.println(p.getImiona()+ " " + p.getNazwisko() +  ": " + "data urodzenia: " + p.getDataUr() + " " + p.getOpis());
        }
        
        Student stu = (Student)ludzie[1];
        
        stu.setSrednia(0.2);
        System.out.println(stu.getImiona()+ " " + stu.getNazwisko() +  ": nowa średnia - " + stu.getSrednia());
    }
}
