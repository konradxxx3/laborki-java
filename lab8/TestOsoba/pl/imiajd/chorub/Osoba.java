package pl.imiajd.chorub;

import java.util.*;
import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.imiona = imiona;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String getImiona(){
        return imiona[0] + " " + imiona[1];
    }
    
    public LocalDate getDataUr(){
        return dataUrodzenia;
    }
    
    public boolean getPlec(){
        return plec;
    }       
    
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
    private String nazwisko;
}
