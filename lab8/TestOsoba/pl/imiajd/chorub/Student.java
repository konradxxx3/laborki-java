package pl.imiajd.chorub;

import java.util.*;
import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + ", średnia: " + sredniaOcen;
    }
    
    public double getSrednia(){
        return sredniaOcen;
    }
    
    public double setSrednia(double wartosc){
        sredniaOcen += wartosc;
        return sredniaOcen;
    }    

    private String kierunek;
    private double sredniaOcen;
}
