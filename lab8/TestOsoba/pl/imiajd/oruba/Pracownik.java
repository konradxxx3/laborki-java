package pl.imiajd.oruba;

import java.util.*;
import java.time.LocalDate;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        String tmp;
        if(super.getPlec() == true)
            tmp = "mężczyzna";
        else
            tmp = "kobieta";
            
        return tmp + ", data zatrudnienia: " + dataZatrudnienia + String.format(", pensja %.2f zł", pobory);
    }
    
    public LocalDate getDataZat(){
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
