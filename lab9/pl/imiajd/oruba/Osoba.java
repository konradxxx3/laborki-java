package pl.imiajd.oruba;

import java.util.*;
import java.time.*;

public class Osoba implements Comparable<Osoba>, Cloneable{
	
	public Osoba(String nazwisko, LocalDate data){
		this.nazwisko=nazwisko;
		this.data=data;
	}
	
	@Override public String toString(){
		return this.getClass().getName()+"[ "+ nazwisko +", "+data+" ]";
	}
	
	@Override public boolean equals(Object other){
		if (this==other) return true;
		if (other==null) return false;
		if (getClass()!=other.getClass()) return false;
		
		Osoba o=(Osoba)other;
		return nazwisko.equals(o.nazwisko) && data.equals(o.data);
	}
	
	public int compareTo(Osoba other){
		int r= nazwisko.compareTo(other.nazwisko);
		if (r==0) r=data.compareTo(other.data);
		return r;
	}
	
	
	/*@Override public int compareTo(Osoba other){
		if (getClass() == other.getClass()) {
			if (nazwisko.compareTo(other.nazwisko) < 0) {
		                return -1;
		        } else if (nazwisko.compareTo(other.nazwisko) > 0) {
                		return 1;
		        } else {
                		return data.compareTo(other.data);
            		}
	        } else {
        		return 1;
	        }
	}*/
	
	
	public Osoba clone() throws CloneNotSupportedException{
		Osoba klon=(Osoba) super.clone();
		//klon.data=(LocalDate) data.clone();
	return klon;
		
	}

	private String nazwisko;
	private LocalDate data;
}
