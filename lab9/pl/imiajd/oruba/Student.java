package pl.imiajd.oruba; 

import java.time.*;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{

	public Student(String nazwisko, LocalDate data, double srednia){
		super(nazwisko,data);
		this.srednia=srednia;
	}
	
	@Override public String toString(){
		return super.toString()+", "+srednia;
	}
	
	public Student clone() throws CloneNotSupportedException{
		Student klon=(Student) super.clone();
		//klon.srednia=srednia;
	return klon;
	}
	
	/*public int compareTo(Student other){
		int r=super.compareTo(other);
		if (r==0) r=(int)(srednia-other.srednia);
	return r;
	}*/
	
	
	public int compareTo(Student other){
		if (super.compareTo(other) == 0 && other instanceof Student) {
	 		if (srednia < ((Student) other).srednia) {
		                return -1;
			} else if (srednia > ((Student) other).srednia) {
				return 1;
			} else {
				return 0;
			}
	        } else if (!(other instanceof Student)) {
        		return -1;
	        } else {
        		return super.compareTo(other);
		}
	}
	
	
	private double srednia;
}
